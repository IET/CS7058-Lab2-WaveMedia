import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.lines import Line2D


frequency 	= 100					#cycles per second (Hertz)
omega 		= 2 * np.pi * frequency	#Angular frequency
amplitude 	= 1
phase     	= 45 					#degrees

c = 343								#propagation speed (m s^-1)
wavelength = c/frequency			#meters
wavenumber = 2 * (np.pi / wavelength) #radians per meter

R = 1.0

# First set up the figure, the axis, and the plot element		 we want to animate

def forward_propagation(R, x, t, k):
	global omega
	return R * np.cos((omega * t) - (k * x))

def backward_propagation(R, x, t, k):
	global omega
	return (1 - R) * np.cos((omega * t) + (k * x))

def combined_wave(R, x, t, k):
	return forward_propagation(R, x, t, k) + backward_propagation(R, x, t, k)


def change_media(R, x, t, in_freq):
	attenuation_factor 	= 0.9	#Due to surface reflection there should be attenuation at the interface
	c_water 			= 1482
	
	wavelength_water	= c_water/in_freq
	wavenumber_water 	= 2 * (np.pi / wavelength_water)

	return combined_wave(R, x, t, wavenumber_water)

#Lots of animation code
class SubplotAnimation(animation.TimedAnimation):
	def __init__(self):
		x0 = 0
		x1 = 20
		x2 = 40

		fig = plt.figure()
		ax1 = fig.add_subplot(1, 2, 1)
		ax2 = fig.add_subplot(1, 2, 2)
		plt.subplots_adjust(wspace = 0)

		self.t = np.linspace(0, 100, 1000)
		self.p = np.linspace(x0, x1, num=250)
		self.q = np.linspace(x1, x2, num=250)

		ax1.set_title("Air")
		ax1.set_xlabel("t")
		ax1.set_ylabel("A")
		self.line1 = Line2D([], [], color="green")
		ax1.add_line(self.line1)
		ax1.set_ylim(-5, 5)
		ax1.set_xlim(x0, x1)

		ax2.set_title("Water")
		self.line2 = Line2D([], [], color="blue")
		ax2.add_line(self.line2)
		ax2.axes.get_yaxis().set_visible(False)
		ax2.set_ylim(-5, 5)
		ax2.set_xlim(x1, x2)

		animation.TimedAnimation.__init__(self, fig, interval=50, blit=True)

	def _draw_frame(self, framedata):
		global wavenumber
		i = framedata/1000
		p = self.p

		air = combined_wave(R, p,i, wavenumber)
		self.line1.set_data(p, air)

		q = self.q

		water = change_media( R, q, i, frequency)
		self.line2.set_data( q, water)

		self.drawn_artists = [self.line1, self.line2]

	def new_frame_seq(self):
		return iter(range(self.t.size))

	def _init_draw(self):
		lines = [self.line1, self.line2]
		for l in lines:
			l.set_data([], [])

ani = SubplotAnimation()

#print ("Saving animation as MPEG4")
#ani.save('sinusoid.mp4', writer='ffmpeg', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()